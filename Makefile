echoServer : echoServer.o thread.o socket.o socketserver.o Blockable.o
	g++ -o echoServer echoServer.o thread.o socket.o socketserver.o Blockable.o -static-libstdc++ -pthread -l rt

Blockable.o : Blockable.cpp Blockable.h
	g++ -c Blockable.cpp -std=c++11
	
echoServer.o : echoServer.cpp socket.h socketserver.h
	g++ -c echoServer.cpp -std=c++11

thread.o : thread.cpp thread.h 
	g++ -c thread.cpp -std=c++11

socket.o : socket.cpp socket.h
	g++ -c socket.cpp -std=c++11

socketserver.o : socketserver.cpp socket.h socketserver.h
	g++ -c socketserver.cpp
	

