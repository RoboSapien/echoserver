#include "socket.h"
#include "socketserver.h"

#include <unistd.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <stdlib.h> 
#include <stdio.h>
#include <thread>



using namespace Communication;

int main(int argc, char** argv)
{
  SocketServer *bob;
  if(argc>=2)
  {
    int port = 1500;
    sscanf (argv[1],"%d",&port);
    bob = new SocketServer(port);
    std::cout << "Listening for connections on port " << atoi(argv[1]) <<"...\n";
  }
  else{
    bob = new SocketServer(1500);
    std::cout << "Listening for connections on port 1500...\n";
  }
  
  
  Socket jim = bob->Accept();
  std::cout << "Got one!\n";
  
  std::string received = "Welcome to the server bub.\n\n";
  
  ByteArray rcvd(received);
  jim.Write(received);
  
  while(received!="done")
  {
    int disconnectCheck = jim.Read(rcvd);
    
    received = rcvd.ToString();
    std::remove(received.begin(), received.end(), '\n');
    std::cout << "Received: " << received << " from the client.\n";
    std::string toSendBack = received;
    toSendBack += "?";
    std::cout << "Sending back " << toSendBack << std::endl;
    toSendBack += "\n";
    ByteArray steve(toSendBack);
    
    jim.Write(steve);
    
    if(disconnectCheck==0)
    {
       std::cout << "Client doen't seem to like us anymore.\n";
       bob->Shutdown();
       jim.Close();
       delete bob;
    }
  }
  
  std::cout << "Server is shutting down...\n";
  
  bob->Shutdown();
}

